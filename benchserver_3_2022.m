A=readmatrix('/mnt/Shared_Data/Benchmarks/LAMMPS_3-2022/results_1.csv')
close all

figure 
hold on


for i =2:7
    plot(A(:,1),A(:,i),'-o','Color',PetersColorMap(i-1),'MarkerFaceColor',PetersColorMap(i-1))
end


A=gca;
% A.YScale='log'
%  axis([0 5e6 -1e6 5e6 ])

xlabel('# threads')
        ylabel('timesteps/s')
        legtext={'New Server (64 cores)','Queen Bee 3 (48 cores)','Old Server (16 cores)','Queen Bee 2 (20 cores)','Desktop (8 core)','Desktop (4-core)'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*1.5)

        legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',12);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        
%         export_fig 'Rt_example' -png -r800 -a1


%% bar
A=readmatrix('/mnt/Shared_Data/Benchmarks/LAMMPS_3-2022/results_1.csv')
X = categorical({'New Server (64 cores)','Queen Bee 3 (48 cores)','Old Server (16 cores)','Queen Bee 2 (20 cores)','Desktop (8 core)','Desktop (4-core)'});
X = reordercats(X,{'New Server (64 cores)','Queen Bee 3 (48 cores)','Old Server (16 cores)','Queen Bee 2 (20 cores)','Desktop (8 core)','Desktop (4-core)'});
Y = [max(A(:,2:end))];
figure
bar(X,Y,'r')

A=gca;
% A.YScale='log'
%  axis([0 5e6 -1e6 5e6 ])

% xlabel('# threads')
        ylabel('timesteps/s')
%         legtext={'New Server (64 cores)','Queen Bee 3 (48 cores)','Old Server (16 cores)','Queen Bee 2 (20 cores)','Desktop (8 core)','Desktop (4-core)'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*1.5)

%         legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',12);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        
%         export_fig 'Rt_example' -png -r800 -a1