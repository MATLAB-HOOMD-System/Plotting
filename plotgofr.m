path='/mnt/Shared_Data/BottleBrush/surfacepolymers/3D/IBI/';
file1='100.dat';
file2='200.dat';

test=1;
i=1;
figure
hold on
while test==1
    if exist(['file' num2str(i)],'var')
        eval(['F' num2str(i) '=dlmread([path file' num2str(i) '],'' '');']);
        eval(['plot(F' num2str(i) '(:,1),F' num2str(i) '(:,2),''Color'',PetersColorMap(i));']);
        if i==1
            legtext={file1};
        else
            eval(['legtext=[legtext,{file' num2str(i) '}];']);
        end
        i=i+1;
    else
        test=0;
    end
end

A=gca;
%  axis([100 230 0 2])

xlabel('r')
        ylabel('g(r)')
%         legtext={filename1,filename2,filename3,filename4,filename5,filename6,filename7,filename8,filename9,filename10}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)

%         legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        
%         export_fig 'Rt_example' -png -r800 -a1