path='/mnt/Shared_Data/BottleBrush/surfacepolymers/2D/';

filename='testBBflat.log';

File1=[path filename];

% copyfile(File1,'temp1.txt')
% copyfile(File2,'temp2.txt')


F1=tdfread(File1)

figure

plot(F1.lx(2:end),F1.pressure(2:end),'k')



A=gca;
% A.YScale='log'
%  axis([0 2.1e7 6.2e5 6.5e5 ])

xlabel('timesteps')
        ylabel('Potential Energy')
        legtext={'No PCND','Xi0.04 Tau100000'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        
%         export_fig 'Rt_example' -png -r800 -a1