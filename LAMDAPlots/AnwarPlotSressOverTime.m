Data=[50 57 70 69 71 75 77 81.5];
x=categorical({'Experiment','Commercially Available','Commercially Unavailable','ML Iteration #1','ML Iteration #2','ML Iteration #3','Manual Tweak','Overall'})
x=categorical({'Experiment','Added Set #1','Added Set #','ML Iteration #1','ML Iteration #2','ML Iteration #3','Manual Tweak','Overall'})

y=categorical(Data)

figure
b=plot(x,Data,'r-','LineWidth',1)
hold on
a=plot(x,Data,'ro','LineWidth',0.5,'MarkerFaceColor','r','MarkerEdgeColor','k','MarkerSize',3)




A=gca;
A.XAxis.Categories=x
% A.YScale='log'
%  axis([1 8 50 85 ])

% xlabel('timesteps')
        ylabel('Recovery Stress (MPa)')
%         legtext={'PE','1AsphaltinPE'}
%         leg=legend(legtext)
        h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
        set(gcf, 'units', 'inches', 'pos', [3 3 3.25/2.2 1.75])

%         legend boxoff
        set(gca,'FontSize',6)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
        xtickangle(45)
        A.LineWidth=0.75;
        pbaspect([1 1 1])
        
%         export_fig 'OverTime' -png -r800 -a1
%         export_fig 'OverTimeTOC' -png -r800 -a1


SHAPData=[16.1156477828746	5.27129196150832
10.3785548097342	4.11806096562672
6.35458286369473	1.96404616959194
5.59603424761552	2.51083500580717
5.51291751526217	3.42196838844543
4.37596605918483	2.28272535272219
4.20647412986778	2.67110767094935
3.82018460565548	2.68945438620914
3.81967251716528	1.37828386185329
2.8911493457908	2.172697195872
2.71266450366835	2.1546408533445
2.66702744116262	1.91141510404075
2.55741888673107	1.10722425019019
1.6704569253353	1.54640890490711
1.64590502418627	0.938175443823813
1.38551008976222	1.0511090128567
1.36806308756326	1.15094201503741
1.35815131769638	1.04226643208791
1.20007899875422	2.1898730898166
1.10252695647032	1.55782331997319
0.746795963392652	0.751463741175028
];

Err=SHAPData(:,2)/10;
x=categorical({'nbackbone';....
'Length Epoxy';....
'polarity (SC)';....
'size (BB)';....
'stretch (BB)';....
'bb ratio';....
'nring (arom)';....
'vdw strength (SC)';....
'Length Hardener';....
'angle (BB)';....
'size (SC)';....
'dihedral (BB)';....
'dihedral (SC)';....
'Rg (epoxy)';....
'Max SC';....
'Rg (hard)';....
'nring (non arom)';....
'stretch (SC)';....
'poalrity (BB)';....
'vdw strength (BB)';....
'angle (SC)';....
})

figure
bar(x,SHAPData(:,1),'r')
hold on
E=errorbar(x,SHAPData(:,1),Err,Err,'Color','k','LineStyle','none')


A=gca;
A.XAxis.Categories=x;
% A.YScale='log'
%  axis([1 8 50 85 ])

% xlabel('timesteps')
        ylabel('Average SHAP Value')
%         legtext={'PE','1AsphaltinPE'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
%         set(gcf, 'units', 'inches', 'pos', [3 3 3.25/2.2 1.75])

%         legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
%         xtickangle(45)
%         A.LineWidth=0.75;
%         pbaspect([1 1 1])
        
%         export_fig 'SHAP' -png -r800 -a1
