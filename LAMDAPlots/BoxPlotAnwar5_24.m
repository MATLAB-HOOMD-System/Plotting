Data1=[81.39	53
80.67	63
195.42	69
194.34	56
67.61	59
66.34	55
56.44	60
56.18	61
]

Data2=[61	57
61	71
70	68
69	59
66	50
66	54
57	39
56	53
]

Data3=[60	75
60	69
69	67
63	59
63	51
63	20
56	45
55	58
]

E1=100*(Data1(:,1)-Data1(:,2))./Data1(:,2)
E2=100*(Data2(:,1)-Data2(:,2))./Data2(:,2)
E3=100*(Data3(:,1)-Data3(:,2))./Data3(:,2)

boxplot([E1,E2,E3],'Whisker',1)


xlabel('ML Iteration #')
ylabel('Percent Prediction Error' )
%         legtext={'T=300 max first peak','T=500 max first peak','T=300 g(10)','T=500 g(10)','T=300 cg(10)','T=500 cg(10)'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*1.5)

        legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        


for i=1:21
    h1.Children.Children(i).LineWidth=2
end

h1.Children.Children(7).Color='k'
h1.Children.Children(8).Color='k'
h1.Children.Children(9).Color='k'


%         export_fig 'Rt_example' -png -r800 -a1