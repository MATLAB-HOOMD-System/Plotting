C=readmatrix("STEALC.txt");
H=readmatrix("STEALH.txt");
L=readmatrix("STEALL.txt");
k=50;
Cx=movmean(C(:,1),k);
Cy=movmean(C(:,2),k);
Hx=movmean(H(:,1),k);
Hy=movmean(H(:,2),k);
Lx=movmean(L(:,1),k);
Ly=movmean(L(:,2),k);

Npart=1;
if Npart==1
    X=[0.135982002747763
0.186806590821424
0.237631178895085
0.288455766968746
0.339280355042407
0.390104943116068
0.440929531189729
0.49175411926339
0.542578707337051
0.593403295410712
0.644227883484373
0.695052471558034
0.745877059631695
0.796701647705356
0.847526235779017
0.898350823852678
0.949175411926339
];
else

X=[0.055555555555556
0.111111111111111
0.166666666666667
0.222222222222222
0.277777777777778
0.333333333333333
0.388888888888889
0.444444444444444
0.5
0.555555555555556
0.611111111111111
0.666666666666667
0.722222222222222
0.777777777777778
0.833333333333333
0.888888888888889
0.944444444444444
];
end
close all
figure
hold on

Y=37*ones(size(X));
plot(50,50,'sm','MarkerFaceColor','m','MarkerSize',10)
plot(X(1:3),Y(1:3),'xr','MarkerFaceColor','r','MarkerSize',10)
plot(X(4:6),Y(4:6),'*b','MarkerFaceColor','b','MarkerSize',10)
plot(X(7:12),Y(7:12),'sg','MarkerFaceColor','g','MarkerSize',10)
% plot(X(13),Y(13),'sg','MarkerSize',10)
plot(X(13:14),Y(13:14),'*b','MarkerFaceColor','b','MarkerSize',10)
plot(X(15:16),Y(15:16),'xr','MarkerFaceColor','r','MarkerSize',10)
plot(X(17),Y(17),'sm','MarkerFaceColor','m','MarkerSize',10)


plot(Cx,Cy,'-k','LineWidth',2);
plot(Hx,Hy,'-k','LineWidth',2);
plot(Lx,Ly,'-k','LineWidth',2);
Gx=[0.41,0.32,0.325,0.34,0.33,0.32];
Gy=[6.3,8,10,13,30,40];
s=spline(Gy,Gx,6.5:0.1:40);
plot(s,6.5:0.1:40,'-k','LineWidth',2);
Gx=[0.41,0.5,0.6,0.65,0.69,0.71,0.725,0.737,0.75];
Gy=[6.3,7,9,11,14,16,19,26,37];
% plot(Gx,Gy,'-ok','LineWidth',2);
s=spline(Gy,Gx,6.5:0.1:40);
plot(s,6.5:0.1:40,'-k','LineWidth',2);

% plot(Cx,Cy,Hx,Hy,Lx,Ly)
% title('Phase Diagram for Star-polymer')
xlabel('f_A')
ylabel('\chiN')
xlim([0 1])
ylim([0 37])

text(0.1,30,'S','FontWeight','bold')
text(0.26,30,'C','FontWeight','bold')
text(0.35,30,'B','FontWeight','bold')
text(0.5,30,'L','FontWeight','bold')
text(0.7,30,'B','FontWeight','bold')
text(0.76,30,'C','FontWeight','bold')
text(0.85,35,'S','FontWeight','bold')

% annotation('arrow',[0.825 0.79],[.78 .82])

% xlabel('timesteps')
%         ylabel('Potential Energy')
        legtext={'Spheres','Cylinders','Bicontinuous','Lamellae'}
        leg=legend(legtext)
        h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
        set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(gcf,'Position',[5267 -303 560 420])
%         export_fig 'Rt_example' -png -r800 -a1
%         export_fig 'Phase_Diagram_Particles_Included' -png -r800 -a1
%         export_fig 'Phase_Diagram_NoParticles' -png -r800 -a1
