File1='S9_2_cohE.log'
File2='S9_2_cohE.log'

copyfile(File1,'temp1.txt')
copyfile(File2,'temp2.txt')


F1=tdfread('temp1.txt')

F2=tdfread('temp2.txt')

close all

plot(F1.timestep,F1.volume,'k')
hold on
plot(F2.timestep,F2.volume,'r')


A=gca;
% A.YScale='log'
%  axis([0 5e6 -1e6 5e6 ])

xlabel('timesteps')
        ylabel('Potential Energy')
        legtext={'PE','1AsphaltinPE'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        
%         export_fig 'Rt_example' -png -r800 -a1
