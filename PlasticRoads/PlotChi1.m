T= [300 400 500 600 700];

chi=[-0.126749518465653
-0.092176146974385
-0.000734097155077
0.012466205733581
-0.011972533955231
];

low95=[-0.201698247262253
-0.144868146510895
-0.037019486808136
-0.021168999707925
-0.033787943597741
];

high95=[-0.051800789669054
-0.039484147437875
0.035551292497981
0.046101411175087
0.009842875687279
];

neg=chi-low95;
pos=high95-chi;



expected=0.028+3.9./T;

figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi,neg,pos,0*neg,0*pos,'-or','LineWidth',1.5)
hold on
plot(T,expected,'bo-','LineWidth',1.5)
h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([250 750 -0.25 0.1])

xlabel('T (K)')
        ylabel('\chi')
        legtext={'MD Results','Literature'}
        leg=legend(legtext,'location','best')
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(h1,'YMinorTick','on')
        
        %export_fig 'Rt_example' -png -r800 -a1
        