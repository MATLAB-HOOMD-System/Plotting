T= [500 600 700];
MW=[5 10 15 20];

chi=[    -0.036277099821222
-0.017728274354755
-0.203939320654245
-0.082928532789022
-0.013157773122089
-0.033794391407504
-0.05267703183799
0.026332784069406
-0.015037760134597
-0.037535925365763
0.011519794005295
0.043715499666867
0.013896045933754
0.035309716313241
-0.023531310847443
0.122493728369524
0.042803723200502
0.02258769676833
0.079560173752453
0.029505579659639
0.049094478956225
0.134726990717639
0.05384855824331
0.051398838995099
-0.125689092374865
-0.091336733573327
-2.5422398231646E-06
0.013153193197986
-0.011267243389096
];








low95=[-0.119565882921623
-0.079087689321503
-0.349113263012438
-0.143654917097082
-0.092762838024624
-0.111743798321998
-0.12581095779377
-0.033451704113457
-0.058254507332956
-0.101792641892813
-0.042907443425893
0.013329097991312
-0.061640259748155
-0.024179969068262
-0.103947623995902
0.067226193528869
-0.027290876851145
-0.016870733314577
0.013804062587719
-0.017779925404637
-0.022740609103732
0.059336024553241
0.003064228885425
0.002122193338596
-0.205184223934946
-0.147225038853144
-0.038489009862334
-0.022522329583452
-0.034405979527968
];

high95=[0.047011683279179
0.043631140611992
-0.058765378296053
-0.022202148480963
0.066447291780446
0.044155015506991
0.020456894117791
0.086117272252268
0.028178987063763
0.026720791161287
0.065947031436482
0.074101901342422
0.089432351615664
0.094799401694745
0.056885002301018
0.17776126321018
0.112898323252148
0.062046126851238
0.145316284917187
0.076791084723915
0.120929567016181
0.210117956882037
0.104632887601194
0.100675484651602
-0.046193960814785
-0.03544842829351
0.038483925382688
0.048828715979423
0.011871492749776
];

neg=chi-low95;
pos=high95-chi;



expected=0.028+3.9./T;

close all
%% PSPMMA 5 vs T
index=1:3;
figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T(index),chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-o','LineWidth',1.5,'Color',AndyColorMap(2))
hold on
% plot(T(index),0.028+3.9./T(index),'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([450 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
% set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/PMMA N=5')
%export_fig 'PSPMMA5' -png -r800 -a1
        
        
%% PSPMMA 10 vs T
index=25:29;
T2=[300 400 500 600 700];
% index=index+3;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T2,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-o','Color',AndyColorMap(3),'LineWidth',1.5)
hold on
% plot(T2,0.028+3.9./T2,'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([250 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
% set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/PMMA N=10')
%export_fig 'PSPMMA10' -png -r800 -a1
        

%% PSPMMA 15 vs T
index=1:3;
index=index+6;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-o','Color',AndyColorMap(4),'LineWidth',1.5)
hold on
% plot(T,0.028+3.9./T,'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([450 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
% set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/PMMA N=15')
%export_fig 'PSPMMA15' -png -r800 -a1
        

%% PSPMMA 20 vs T
index=1:3;
index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-o','Color',AndyColorMap(5),'LineWidth',1.5)
hold on
plot(T,0.028+3.9./T,'o-','Color',AndyColorMap(6),'LineWidth',1.5)
h3=plot([0 1000],[0 0],'--k','HandleVisibility','off');
axis([250 750 -0.25 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'N = 5','N = 10','N = 15','N = 20','Literature'};
leg=legend(legtext,'location','northwest','NumColumns',2);
% leg=columnlegend(2,legtext)
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
leg.Position=[0.2204 0.7008 0.3206 0.1088];
legend boxoff
set(gca,'FontSize',14)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',12);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/PMMA')
export_fig 'PSPMMA-T' -png -r800 -a1

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PMMA vs T

%% PSPMMA 500K
index=[1 4 7 10];
% index=index+9;
figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(2))
hold on
% plot(MW,(0.028+3.9./500)*ones(1,4),'bo-','LineWidth',1.5)
% h3=plot([0 25],[0 0],'--k','HandleVisibility','off')
axis([0 25 -0.2 0.2])

xlabel('N')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',14)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',12);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/PMMA')
%export_fig 'PSPMMA500K' -png -r800 -a1

%% PSPMMA 600K
index=[1 4 7 10]+1;
% index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(3))
hold on
% plot(MW,(0.028+3.9./600)*ones(1,4),'bo-','LineWidth',1.5)
% h3=plot([0 25],[0 0],'--k','HandleVisibility','off')
axis([0 25 -0.2 0.2])

xlabel('N')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')

title('PS/PMMA 600K')
%export_fig 'PSPMMA600K' -png -r800 -a1

%% PSPMMA 700K
index=[1 4 7 10]+2;
% index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(4))
hold on
plot(MW,(0.028+3.9./600)*ones(1,4),'bo-','LineWidth',1.5,'Color',AndyColorMap(6))
h3=plot([0 25],[0 0],'--k','HandleVisibility','off');
axis([0 25 -0.2 0.2])

xlabel('N')
ylabel('\chi')
% legtext={'MD Results','Literature'}
legtext={'T = 500 K','T = 600 K','T = 700 K','Literature T = 600 K'};
leg=legend(legtext,'location','northeast');
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
leg.Position=[0.2479 0.7210 0.2881 0.1430];
legend boxoff
set(gca,'FontSize',14)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',12);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')

title('PS/PMMA')
export_fig 'PSPMMA-MW' -png -r800 -a1

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%AAC1 vs MW

%% PSAAC1 500K
index=[13:3:24];
% index=index+9;
figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5)
hold on
% plot(MW,(0.028+3.9./700)*ones(1,4),'bo-','LineWidth',1.5)
% h3=plot([0 25],[0 0],'--k','HandleVisibility','off')
axis([0 25 -0.2 0.2])

xlabel('N_{PS}')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','southeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')

title('PS/AAC-1 500K')
%export_fig 'PSAAC-1_500K' -png -r800 -a1

%% PSAAC1 600K
index=[13:3:24]+1;
% index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(3))
hold on
% plot(MW,(0.028+3.9./700)*ones(1,4),'bo-','LineWidth',1.5)
% h3=plot([0 25],[0 0],'--k','HandleVisibility','off')
axis([0 25 -0.2 0.2])

xlabel('N_{PS}')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','southeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')

title('PS/AAC-1 600K')
%export_fig 'PSAAC-1_600K' -png -r800 -a1

%% PSAAC1 700K
index=[13:3:24]+2;
% index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(MW,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(4))
hold on
% plot(MW,(0.028+3.9./700)*ones(1,4),'bo-','LineWidth',1.5)
h3=plot([0 25],[0 0],'--k','HandleVisibility','off');
axis([0 25 -0.2 0.2])

xlabel('N_{PS}')
ylabel('\chi')
legtext={'T = 500 K','T = 600 K','T = 700 K'};
leg=legend(legtext,'location','southeast');
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
leg.Position=[0.5517 0.2729 0.1813 0.1088];
legend boxoff
set(gca,'FontSize',14)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',12);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')

title('PS/AAC-1')
export_fig 'PSAAC1-MW' -png -r800 -a1


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%AAC1 vs T

%% PS5-AAC1 vs T
index=1:3;
index=index+12;
figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(2))
hold on
% plot(T,0.028+3.9./T,'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([450 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','southwest')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/AAC-1 N_{PS}=5')
%export_fig 'PS5-AAC1' -png -r800 -a1
        
        
%% PS10-AAC1 vs T
T2=[300:100:700];
chi2=[0.044122397857624
0.016286221272986
0.043464985304053
0.029629915265108
0.031064577079436
];
chilow2=[-0.02764295377832
-0.036524765143984
0.008559849549254
-0.006773739474359
0.005602623462442
];
chihigh2=[0.115887749493568
0.069097207689956
0.078370121058853
0.066033570004575
0.056526530696429
];
neg2=chi2-chilow2;
pos2=chihigh2-chi2;
% index=1:3;
% index=index+15;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T2,chi2,neg2,pos2,0*neg2,0*pos2,'-or','LineWidth',1.5,'Color',AndyColorMap(3))
hold on
% plot(T,0.028+3.9./T,'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([250 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/AAC-1 N_{PS}=10')
%export_fig 'PS10-AAC1' -png -r800 -a1
        

%% PS15-AAC1 vs T
index=1:3;
index=index+18;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(4))
hold on
% plot(T,0.028+3.9./T,'bo-','LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
axis([450 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'MD Results','Literature'};
% leg=legend(legtext,'location','northeast')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',9);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/AAC-1 N_{PS}=15')
%export_fig 'PS15-AAC1' -png -r800 -a1
        

%% PS20-AAC1 vs T
index=1:3;
index=index+9;
% figure
% plot(T,chi,'ro-','LineWidth',2)
errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-or','LineWidth',1.5,'Color',AndyColorMap(5))
hold on
% plot(T,0.028+3.9./T,'bo-','LineWidth',1.5)
h3=plot([0 1000],[0 0],'--k','HandleVisibility','off');
axis([250 750 -0.2 0.2])

xlabel('T (K)')
ylabel('\chi')
legtext={'N = 5','N = 10','N = 15','N = 20'};
leg=legend(legtext,'location','northeast','NumColumns',2);
h1=gca;
set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
leg.Position=[0.1718 0.3034 0.2920 0.0746];
legend boxoff
set(gca,'FontSize',14)
set(gcf,'color','w');
set(gca,'color','None');
box on
set(leg,'FontSize',12);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
set(h1,'YMinorTick','on')
title('PS/AAC-1')
export_fig 'PSAAC1-T' -png -r800 -a1

% %% PSPMMA 20 vs T
% index=1:3;
% index=index+9;
% % figure
% % plot(T,chi,'ro-','LineWidth',2)
% errorbar(T,chi(index),neg(index),pos(index),0*neg(index),0*pos(index),'-o','Color',AndyColorMap(5),'LineWidth',1.5)
% hold on
% plot(T,0.028+3.9./T,'o-','Color',AndyColorMap(6),'LineWidth',1.5)
% h3=plot([0 1000],[0 0],'--k','HandleVisibility','off')
% axis([250 750 -0.25 0.2])
% 
% xlabel('T (K)')
% ylabel('\chi')
% legtext={'N = 5','N = 10','N = 15','N = 20','Literature'}
% leg=legend(legtext,'location','northwest','NumColumns',2)
% % leg=columnlegend(2,legtext)
% h1=gca;
% set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3]*2)
% 
% legend boxoff
% set(gca,'FontSize',10)
% set(gcf,'color','w');
% set(gca,'color','None');
% box on
% set(leg,'FontSize',9);
% set(h1,'TickLength',[.02 .1])
% set(h1,'XMinorTick','on')
% set(h1,'YMinorTick','on')
% title('PS/PMMA')
% %export_fig 'PSPMMA20' -png -r800 -a1
