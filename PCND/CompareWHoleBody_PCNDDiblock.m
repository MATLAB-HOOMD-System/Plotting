close all
clear all

N32=[5.9785, 106.0517
4.8535, 129.92
4.252, 167.2588
2.4334, 224.0926
1.8103, 431.132
1.1867, 846.0915
0.8114, 3972.8591
0.7612, 4261.3312];

N24=[4.3865, 120.773
3.8558, 128.509
3.5303, 145.0217
3.0775, 144.3812
2.2141, 219.539
1.7327, 341.9133
1.3078, 494.0704
0.875, 3981.4357
0.839, 4282.6576];

N16=[2.7945, 148.2241
2.5326, 177.5567
1.9594, 214.9355
1.6762, 278.1842
1.3998, 502.6871
0.967, 3973.0793
0.938, 4299.7708];


numforces=(60*8+1);
Wholebody=[0.034*numforces 3000
    0.032*numforces 6000
    0.026*numforces 10000
    0.02*numforces 30000
    0.012*numforces 60000
    0.01*numforces 100000
    0.008*numforces 300000
    0.008*numforces 1000000
    0.008*numforces 3000000
    0.008*numforces 10000000]

% Wholebody=[0.034*numforces 3000
%     0.032*numforces 6000
%     0.028*numforces 10000
%     0.02*numforces 30000
%     0.016*numforces 60000
%     0.014*numforces 100000
%     0.012*numforces 300000
%     0.012*numforces 1000000
%     0.008*numforces 3000000
%     0.008*numforces 10000000]


figure1=figure;

hold on
% plot(N16(:,1),N16(:,2),'-k','LineWidth',3)
% plot(N24(:,1),N24(:,2),'-r','LineWidth',3)
plot(N32(:,1),N32(:,2),'-b','LineWidth',3)

% xlabel('(N_{beads}-2)\times\xi')
xlabel('F_{Tot,Linear}');
ylabel('\tau')
% leg=legend({'N_{beads}=16','N_{beads}=24','N_{beads}=32'});
leg.Position=[0.5386    0.5490    0.2129    0.1646];
set(gca,'FontSize',22)
set(gcf,'Color','w')
set(gcf,'Position',[2003         906         834         741])
a1=gca;
a1.XTick=[0 2 4 6];
% a1.YTick=[0:1000:4000];
a1.YScale='log';
a1.XScale='lin';
a1.LineWidth=2;
a1.Position=[0.1666    0.1756    0.7    0.7]
% box on

ax1_pos = a1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');

xlabel('(N_{beads}-2)\times\xi')
ylabel('\tau')
% leg=legend({'N_{beads}=16','N_{beads}=24','N_{beads}=32'});
legend='Whole Body Force'
leg.Position=[0.5386    0.5490    0.2129    0.1646];
set(gca,'FontSize',22)
% set(gcf,'Color','w')
set(gcf,'Position',[2003         906         834         741])
a2=gca;
a2.XTick=[0 2 4 6];
% a1.YTick=[0:1000:4000];
a2.YScale='log';
a2.XScale='lin';
a2.LineWidth=2;
a2.XLim=[0 0.036]*numforces
a2.YLim=[1000 3e7];
% a2.YLim=[1000 10e7];
a1.YLim=[50 10000]
a2.XTick=[0:3:21]
a1.XColor='b'
a1.YColor='b'
a2.XColor='r'
a2.YColor='r'
% a2.XLabel.String='N_{forces}\times\xi'
a2.XLabel.String='F_{Tot,Whole Body}'


line(Wholebody(:,1),Wholebody(:,2),'Parent',a2,'Color','r','LineWidth',3)


% export_fig -r200 -a2 PCNDreplot_log.png