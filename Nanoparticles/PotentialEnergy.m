close all
filename='PCND_Nano_XiNano0.016_tNano30000_XiPoly0_tPoly0_longquarter1.log';
filepath='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Refine_PCND_Nano/TestXi0results/testbestsetsagain/';

filepath2='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Newruns_4_18_18/Nr2_numgrafts60_lengthgrafts18_fracA_0p5/';
filename2='PCND_Nano_NoPCND_long.log';

filename3='/home/andrew/simulationtesting/continueNoPCNDforPaper/PCND_Nano_XiNano0.016_tNano30000_XiPoly0_tPoly0_ContNoPCND1.log';

Data2=dlmread([filepath2 filename2],'	',1,0);
time2=Data2(:,1);
U2=Data2(:,4);
plot(time2,U2,'k')

hold on

Data=dlmread([filepath filename],'	',1,0);
time=Data(:,1);
U=Data(:,4);
plot(time,U,'r')





xlabel('timesteps')
ylabel('U')
leg=legend({'No PCND','\xi=0.016, \tau=30000'});
leg.Position=[0.5190    0.6826    0.3193    0.1646];
set(gca,'FontSize',12)
set(gcf,'Color','w')
set(gcf,'Position',[2003         906         834         741])
a1=gca;
axis([0 10e6 6.3e5 6.6e5])
% a1.XTick=[0 2 4 6];
% a1.YTick=[0:1000:4000];
% a1.YScale='log';
% a1.XScale='log';
% a1.LineWidth=2;
box on






%Calc equilibration time
start=find(time>=2e6,1);
final=find(time>=4e6,1);
Plat=mean(U(start:final));
U2=movmean(U,101);
start2=find(time>=15000,1);
U3=U2(start2:end);
equil=time(find(U3<Plat,1)+start2);

text(4e6,6.35e5,'Turn off PCND','Color','r')
text(1.5e6,6.52e5,'Forms Lamellae','Color','r')

annotation('arrow',[0.327338129496403 0.223021582733813],...
    [0.685909581646424 0.545209176788124]);
annotation('arrow',[0.515587529976019 0.516786570743405],...
    [0.260808367071525 0.33468286099865]);


% export_fig -r200 -a2 PotentialEnergy.png




Data3=dlmread([filename3],'	',1,0);
time3=Data3(:,1);
U3=Data3(:,4);

PU3=plot(time3,U3,'r')
