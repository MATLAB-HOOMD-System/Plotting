clear all
close all
filename='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Results Summary.xlsx';
sheet='FixedPCND, refinePCND';
xlRange='F49:P63';



num = xlsread(filename,sheet,xlRange)

num(end-2:end-1,:)=[];
num(isnan(num))=10e6;
% num(num==10e6)=NaN;
% X=[];
% Y=[];
% Z=[];


% for i=2:size(num,1)
%     for j=2:size(num,2)
%         if ~isnan(num(i,j))
%             X(end+1)=num(i,1);
%             Y(end+1)=num(1,j);
%             Z(end+1)=num(i,j);
%         end
%     end
% end


Z=num(2:end,2:end)
Y=num(2:end,1)
X=num(1,2:end)

% Y=[0; Y];
% Z=[10e6*ones(1,11); Z];
%% colormap

ZZ=log10(Z);
Cs=flipud(hot(101));
M=max(max(ZZ));
m=min(min(ZZ));
ZZ=round(100*(ZZ-m)/(M-m));
for i=1:size(ZZ,1)
    for j=1:size(ZZ,2)
        C(i,j,:)=Cs(ZZ(i,j)+1,:);
    end
end


%%

%colors
% Cs=hot(1000);
% Cs(1,:)=[1 1 1];
% for i=1:size(Z,1)
%     for j=1:size(Z,2)
%         C(i,j,:)=Cs(1000-round((Z(i,j)/10000000)*1000)+1,:);
%     end
% end


% s=surf(X,Y,Z,C)
% set(gca, 'XScale', 'log')

% s.AlphaData = (Z<9000000);
% s.FaceColor = 'texturemap';
% s.FaceAlpha = 'texturemap';
% s.EdgeColor = 'none';

% figure
% hold on
% 
% for i=1:size(Z,1)
%     plot3(Y(i)*ones(size(X)),X,Z(i,:))
% end
% set(gca, 'YScale', 'log')

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create surf
surf(X,Y,Z,C,'FaceColor','interp');

view(axes1,[39.3000   61.2000]);
grid(axes1,'on');
% Set the remaining axes properties
set(axes1,'XMinorTick','on','XScale','log','ZDir','reverse','ZMinorTick',...
    'on','ZScale','log');
colormap(flipud(hot))
set(gcf,'Position',[2003         393        1516        1254])
xlabel('\tau','fontweight','bold')
ylabel('\xi','fontweight','bold')
zlabel('Equilibration Time','fontweight','bold')
set(gca,'FontSize',20)
set(gcf,'Color','w')


% view(axes1,[90 90]);
% a=gca;
% a.YAxisLocation= 'right'

c=colorbar;
set(c,'YDir','reverse');
Zlog=log10(Z);
minZlog=log10(5e5);
c.Ticks=[0 (6-minZlog)/(max(max(Zlog))-minZlog) (log10(2500000)-minZlog)/(max(max(Zlog))-minZlog) (log10(5000000)-minZlog)/(max(max(Zlog))-minZlog) 1];
c.TickLabels={'5\times10^5','10^6','2.5\times10^6','5\times10^6','10^7'}
% c.TickLabels={5,1,1}

a=gca;
a.Position=[0.1221 0.1100 0.7078 0.8150];
c.Position=[0.900 0.1100 0.0174 0.8150];

h = get(gca, 'xlabel')
h.Position=[1e5,-.006,1e7];
h = get(gca, 'ylabel')
h.Position=[2.3476e7,0.024,2.24e7];



% view(axes1,[3.3000    3.6000]);
% 
% h = get(gca, 'xlabel')
% h.Position=[1e+05 1.6375e-04 1.5730e+07];
% h = get(gca, 'ylabel')
% h.Position=[2.5095e+07 0.0200 1.4142e+07];
