Xival=1;
Tauval=0;
viewrot=1;
[axes1,c,figure1]=Plot_times_NanoparticlePCND(0,0)
% clearvars -except axes1 figure1 c
% clear all
% close all
filename='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Results Summary.xlsx';
sheet='FixedPCND, refinePCND';
xlRange='F49:P63';



num = xlsread(filename,sheet,xlRange)

num(end-2:end-1,:)=[];
num(isnan(num))=10e6;
% num(num==10e6)=NaN;
% X=[];
% Y=[];
% Z=[];


% for i=2:size(num,1)
%     for j=2:size(num,2)
%         if ~isnan(num(i,j))
%             X(end+1)=num(i,1);
%             Y(end+1)=num(1,j);
%             Z(end+1)=num(i,j);
%         end
%     end
% end


Z=num(2:end,2:end)
Y=num(2:end,1)
if Xival==1
    Y=Y*0.22375/1.732;
end
X=num(1,2:end)
if Tauval==1
%     X=X*0.1187/3;
    X=0.1*X;
end
% 
% Y=Y(1:end-3);
% Z=Z(1:end-3,:);
% Z(end+1,:)=1e7*ones(1,10);
% % Z(end,end)=NaN;
% Y(end+1)=0.014;

% Y=[0; Y];
% Z=[10e6*ones(1,11); Z];
%% colormap

ZZ=log10(Z);
Cs=flipud(bone(101));
M=max(max(ZZ));
M=7;
m=min(min(ZZ));
m=log10(5e5);
ZZ=round(100*(ZZ-m)/(M-m));
for i=1:size(ZZ,1)
    for j=1:size(ZZ,2)
        C(i,j,:)=Cs(ZZ(i,j)+1,:);
    end
end


%%

%colors
% Cs=hot(1000);
% Cs(1,:)=[1 1 1];
% for i=1:size(Z,1)
%     for j=1:size(Z,2)
%         C(i,j,:)=Cs(1000-round((Z(i,j)/10000000)*1000)+1,:);
%     end
% end


% s=surf(X,Y,Z,C)
% set(gca, 'XScale', 'log')

% s.AlphaData = (Z<9000000);
% s.FaceColor = 'texturemap';
% s.FaceAlpha = 'texturemap';
% s.EdgeColor = 'none';

% figure
% hold on
% 
% for i=1:size(Z,1)
%     plot3(Y(i)*ones(size(X)),X,Z(i,:))
% end
% set(gca, 'YScale', 'log')

% Create figure
% figure1 = figure;

% Create axes
% axes1 = axes('Parent',figure1);
% hold(axes1,'on');

% Create surf
hold on
pause(0.1) %fizes color bar for some reason
surf(X,Y,Z,C,'FaceColor','interp');

view(axes1,[39.3000   61.2000]);
grid(axes1,'on');
% Set the remaining axes properties
set(axes1,'XMinorTick','on','XScale','log','ZDir','reverse','ZMinorTick',...
    'on','ZScale','log');
% colormap(flipud(bone))
set(gcf,'Position',[2003         393        1516        1254])
xlabel('\tau')
ylabel('\xi')
zlabel('\tau_{equil}')
set(gca,'FontSize',20)
set(gcf,'Color','w')


% view(axes1,[90 90]);
% a=gca;
% a.YAxisLocation= 'right'

a=gca;
a.Position=[0.1 0.1100 0.66 0.8];
c.Position=[0.840 0.1100 0.0174 0.8150];


h = get(gca, 'xlabel')
h.Position=[1e5,-0.03,1e7];
h = get(gca, 'ylabel')
h.Position=[2.3476e8,0.12,2.24e7];
% axes1.YLim=[0 0.1];
if Xival==1 
    axes1.YLim=[0 0.04]
    axes1.XLim=[10^2 10^8]
    h = get(gca, 'xlabel')
    h.Position=[1e5,-0.007,1e7];
    h = get(gca, 'ylabel')
    h.Position=[1.5*10^8,0.022,1e7];
    h.String='\xi_{\iteff}';
    pause(0.01)
    c.Position=[0.840 0.1100 0.0174 0.8150];
    pause(0.01)
%     axes1.YLim=[0 0.2]
    axes1.XLim=[10^3 3*10^7]
%     axes1.YTick=[0:0.05:2];
    axes1.XTick=[10.^[3:8]];
    if viewrot==1
        view(axes1,[90   90]);
        h = get(gca, 'xlabel')
        h.Position=[3e5,-0.004,1e7];
        h = get(gca, 'ylabel')
        h.Position=[5e7,0.02,1e7];
        h.String='\xi_{\iteff}';
    end
else
    axes1.YLim=[0 0.2]
    axes1.XLim=[10^3 3*10^7]
    axes1.YTick=[0:0.05:2];
    axes1.XTick=[10.^[3:8]];
%     axes1
    if viewrot==1
        view(axes1,[45   0]);
        h = get(gca, 'xLabel')
        delete(h)
        h=xlabel('\tau');
        h.Position(1)=3e6;
        h = get(gca, 'yLabel')
        delete(h)
        ylabel('\xi');
        text(1e6,0.15,2e6,{'Polymer Arm','Force'},'Color',[166 198 198 ]/255,'FontSize',20','horizontalAlignment', 'center');
        text(1e5,0,3e5,{'Whole Body','Force'},'Color',[240 150 96]/255,'FontSize',20,'horizontalAlignment', 'center');
        g=gca;
        g.Position=[0.1100    0.1100    0.6600    0.8000]
    %     h.Position=[1e5,-0.007,1.3e7];
    %     h = get(gca, 'ylabel')
    %     h.Position=[10e8,0.02,1.3e7];
    else
        text(3e5,0.215,6e6,{'Polymer Arm','Force'},'Color',[166 198 198 ]/255,'FontSize',20','horizontalAlignment', 'center');
        text(5e3,-0.025,6e7,{'Whole','Body','Force'},'Color',[240 150 96]/255,'FontSize',20,'horizontalAlignment', 'center');
        
        
        
    end
%     h = get(gca, 'xlabel')
%     h.Position=[1e5,-0.02,1e7];
%     h = get(gca, 'ylabel')
%     h.Position=[2e8,0.1,1e7];
end
axes2 = axes('Parent',figure1,'visible','off');
set(axes2,'FontSize',20)
colormap(axes2,flipud(bone));
c2=colorbar;
set(c2,'YDir','reverse');
c2.Position=[0.8600 0.1100 0.0174 0.8150];
Zlog=log10(Z);
minZlog=log10(5e5);
maxZlog=log10(1e7);
c2.Ticks=[0 (6-minZlog)/(maxZlog-minZlog) (log10(2500000)-minZlog)/(maxZlog-minZlog) (log10(5000000)-minZlog)/(maxZlog-minZlog) 1];
c2.TickLabels={'5\times10^5','10^6','2.5\times10^6','5\times10^6','10^7'}
c.TickLabels={'','','',''}
% c.Position=[0.82500 0.1100 0.0174 0.8150];
% c.TickLabels={5,1,1}
% axes1.YScale='log'




% view(axes1,[36.5000   0]);
% 
% h = get(axes1, 'xlabel')
% h.Position=[1e+05 1.6375e-04 1.4e+07];
% h = get(axes1, 'ylabel')
% h.Position=[2.5095e+07 0.150 1.4e+07];

% export_fig -r200 -a2 PCNDNano_1.png
