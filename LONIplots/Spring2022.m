% File1='/mnt/Shared_Data/PlasticRoads/ProposalImage/asphalt.log'
% File2='/mnt/Shared_Data/PlasticRoads/ProposalImage/1PE_inAsphalt.log'
% 
% copyfile(File1,'temp1.txt')
% copyfile(File2,'temp2.txt')
% 
% 
% F1=tdfread('temp1.txt')
% 
% F2=tdfread('temp2.txt')


Data1=[1	0.389
2	0.408
4	1
8	2.445
16	5.549
24	7.637
48	13.511
];

Data2=[1	13.511
2	24.978
4	44.684
8	72.355
16	114.952

];



close all

plot(Data1(:,1),Data1(:,2),'k-o')
% hold on
% plot(F2.timestep,F2.potential_energy,'r')


A=gca;
% A.YScale='log'
%  axis([0 5e6 -1e6 5e6 ])

xlabel('Cores')
        ylabel('Timesteps Per Second')
%         legtext={'PE','1AsphaltinPE'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(h1,'XScale','log')
%         set(h1,'YScale','log')
%         export_fig 'Rt_example' -png -r800 -a1


figure
plot(Data2(:,1),Data2(:,2),'k-o')
% hold on
% plot(F2.timestep,F2.potential_energy,'r')

A=gca;
% A.YScale='log'
%  axis([0 5e6 -1e6 5e6 ])

xlabel('Nodes')
        ylabel('Timesteps Per Second')
%         legtext={'PE','1AsphaltinPE'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',12)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',9);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(h1,'XScale','log')
%         set(h1,'YScale','log')
%         export_fig 'Rt_example' -png -r800 -a1