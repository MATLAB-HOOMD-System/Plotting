% [hd,hcb]=ternaryc(c1,c2,c3,d,symbol)

  close all;clear all
  warning off MATLAB:griddata:DuplicateDataPoints
  load limestone
  
  A=[0.1	0.2	0.7	3
0.1	0.4	0.5	2
0.1	0.6	0.3	4
0.1	0.8	0.1	1
0.2	0.2	0.6	5
0.2	0.4	0.4	2
0.2	0.6	0.2	4
0.3	0.2	0.5	2
0.3	0.4	0.3	2
0.3	0.6	0.1	4
0.4	0.2	0.4	2
0.4	0.4	0.2	2
0.7	0.2	0.1	3
0.5	0.4	0.1	2
0.3	0.6	0.1	4
0.1	0.8	0.1	1
0.6	0.2	0.2	5
0.4	0.4	0.2	2
0.2	0.6	0.2	4
0.5	0.2	0.3	2
0.3	0.4	0.3	2
0.1	0.6	0.3	4
0.4	0.2	0.4	2
0.2	0.4	0.4	2



]
  % (The data file comes with the zip file).
  figure
  % Plot the ternary axis system
  [h,hg,htick]=terplot;
  % Plot the data
  % First set the colormap (can't be done afterwards)
%   colormap(jet)
colormap=[1 0 0;0 0 1;0 1 0;0.5 0 0.5;0 0.5 0.5;0 0 0];
colormap=AndyColorMap(2:6)
  [hd,hcb]=ternaryc(A(:,1),A(:,2),A(:,3),A(:,4),'o',20,colormap);
  %   or
%   [hd]=ternaryc(A(:,1),A(:,2),A(:,3));
  %   for a constant value ternary plot.
  % Add the labels
  hlabels=terlabel('A','BC','D');