% [hd,hcb]=ternaryc(c1,c2,c3,d,symbol)

  close all;clear all
  warning off MATLAB:griddata:DuplicateDataPoints
%   load limestone
  
  %1 = spheres undefined
  %2 bicont undefined
  % 3 perf lam
  % 4 lam
  % 5 mixed
  %6 cylinders
  
  A=[[1 2 17]/(1+2+17) 1;
      [1 4 15]/(1+4+15) 2;
      [1 6 13]/(1+6+13) 3;
      [1 8 11]/(1+8+11) 4;
      [1 10 9]/(1+10+9) 4;
      [1 12 7]/20 2;
      [1 14 5]/20 2;
      [1 16 3]/20 1;
      [1 18 1]/20 5;
      [2 2 16]/20 1;
      [2 4 14]/20 2;%layered on gyroid
      [2 6 12]/20 2;%layered on gyroid
      [2 8 10]/20 4;%layerd on lam
      [2 10 8]/20 2;%layered on gyroid
      [2 12 6]/20 2;
      [2 14 4]/20 2;%double gyrdoid
      [2 16 2]/20 2;%double gyroid
      [3 2 15]/20 1;%strange sizes, layered
      [3 4 13]/20 3;% layered on lam
      [3 6 11]/20 4;%layered
      [3 8 9]/20 3; %layered on A
      [3 10 7]/20 2; %layered on A
      [3 12 5]/20 2; %layered on A
      [3 14 3]/20 2; %A+D phase
      [3 16 1]/20 1;%D mixed
      [4 2 14]/20 6;% large, AC coating
      [4 4 12]/20 4;
      [4 6 10]/20 4;%very defective
      [4 8 8]/20 4;
      [4 10 6]/20 3;
      [4 12 4]/20 2;%A+D phase
      [4 14 2]/20 2; %A+D phase
      [5 2 13]/20 2; %AD gyroid, BC is just along for the ride at the interface
      [5 4 11]/20 4;
      [5 6 9]/20 4; %very defective
      [5 8 7]/20 4;
      [5 10 5]/20 4;%alternating lamellae
      [5 12 3]/20 2; %not really sure
      [5 14 1]/20 6; %unclear
      [6 2 12]/20 4;
      [6 4 10]/20 4; %perforated?
      [6 6 8]/20 4;
      [6 8 6]/20 4;
      [6 10 4]/20 2;%all gyroid
      [6 12 2]/20 2;%sphere decorated gyroid?
      [7 2 11]/20 4;
      [7 4 9]/20 4;
      [7 6 7]/20 4;
      [7 8 5]/20 4;
      [7 10 3]/20 2; % All gyroid sort of. D has some spheres
      [7 12 1]/20 2;
      [8 2 10]/20 4; %Very poorly formed.
      [8 4 8]/20 4;
      [8 6 6]/20 4;
      [8 8 4]/20 4;
      [8 10 2]/20 2; %D cylinders on A, in BC
      [9 2 9]/20 4;
      [9 4 7]/20 4;
      [9 6 5]/20 4;
      [9 7 3]/20 2; %BC coating A
      [9 10 1]/20 4;
      [10 2 8]/20 4;
      [10 4 6]/20 4;
      [10 6 4]/20 4;
      [10 8 2]/20 2; %BC coating D, but D mostly (not all) formed gyroid
      [11 2 7]/20 4;
      [11 4 5]/20 4;
      [11 6 3]/20 3;
      [11 8 1]/20 4;
      [12 2 6]/20 4;
      [12 4 4]/20 4; %pretty bad, maybe perf?
      [12 6 2]/20 2; %BC coating D
      [13 2 5]/20 2; %maybe cylinders
      [13 4 3]/20 2; %BC coating D all gyroid
      [13 6 1]/20 4;
      [14 2 4]/20 2; %maybe cylinders but they're too large
      [14 4 2]/20 2; %all gyroid, BC coating D\
      [15 2 3]/20 1; %BC coating D
      [15 4 1]/20 2; %D sorta in BC
      [16 2 2]/20 1; %BC coating D
      [17 2 1]/20 1; %BC coatoing D (mostly BC)
      
]   
  % (The data file comes with the zip file).
  figure
  % Plot the ternary axis system
  [h,hg,htick]=terplot;
  % Plot the data
  % First set the colormap (can't be done afterwards)
%   colormap(jet)
colormap=[1 0 0;0 0 1;0 1 0;0.5 0 0.5;0 0.5 0.5;0 0 0];
colormap=AndyColorMap([1 2 3 6 4 5 7])
  [hd,hcb]=ternaryc(A(:,1),A(:,3),A(:,2),A(:,4),'o',20,colormap);
  %   or
%   [hd]=ternaryc(A(:,1),A(:,2),A(:,3));
  %   for a constant value ternary plot.
  % Add the labels
  hlabels=terlabel('A','D','BC');
    colorbar('off')