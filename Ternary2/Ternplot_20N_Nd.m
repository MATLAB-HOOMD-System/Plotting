% [hd,hcb]=ternaryc(c1,c2,c3,d,symbol)

  close all;clear all
  warning off MATLAB:griddata:DuplicateDataPoints
  load limestone
  
  A=[

]
  % (The data file comes with the zip file).
  figure
  % Plot the ternary axis system
  [h,hg,htick]=terplot;
  % Plot the data
  % First set the colormap (can't be done afterwards)
%   colormap(jet)
colormap=[1 0 0;0 0 1;0 1 0;0.5 0 0.5;0 0.5 0.5;0 0 0];
colormap=AndyColorMap([1 2 3 6 4 5 7])
  [hd,hcb]=ternaryc(A(:,1),A(:,3),A(:,2),A(:,4),'o',20,colormap);
  %   or
%   [hd]=ternaryc(A(:,1),A(:,2),A(:,3));
  %   for a constant value ternary plot.
  % Add the labels
  hlabels=terlabel('A','D','BC');
    colorbar('off')