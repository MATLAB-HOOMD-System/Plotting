function linespec=CycleLineStyle(num,type)
    if num<1 || floor(num)~=num
        error('invalid number input')
    end
    if ~exist('type','var')
        type=0;
    end
    linestyle={'-','--',':','-.'};
    markertype = 'osdx+^v<>*ph';
    if type ==0 %cycle separatley
        l=rem(num-1,length(linestyle))+1;
        m=rem(floor((num-1)/length(linestyle)),length(markertype))+1;
        linespec=[linestyle{l} markertype(m)];
    elseif type==1 %cycle together
        l=rem(num-1,length(linestyle))+1;
        m=rem(num-1,length(markertype))+1;
        linespec=[linestyle{l} markertype(m)];
    end
end